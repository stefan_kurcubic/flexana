(ns flex.server.scramble)


(defn scramble?
  "Returns true if a portion of `str1` can be rearanged to patch `str2`,
  otherwise returns false."
  [str1 str2]
  (let [freq1 (frequencies str1)
        freq2 (frequencies str2)]
    (every? (fn [[k v]]
              (<= v (get freq1 k 0)))
            freq2)))

(defn valid? [string]
  (and (every? #(Character/isLowerCase %) (seq string))
     (not (empty? string))))

(defn try-to-scramble
  "Checks if received strings are valid and based on their validity
  returns appropriate response"
  [str1 str2]
  (if (and (valid? str1) (valid? str2))
    (str (scramble? str1 str2))
    {:status 400}))
