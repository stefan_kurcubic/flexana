(ns flex.server.main
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.adapter.jetty :as jetty]
            [flex.server.scramble :as scr]))

(defroutes app-routes
  (GET "/scramble" [str1 str2] (scr/try-to-scramble str1 str2))
  (route/not-found "Not Found"))

(def app
  (wrap-defaults app-routes site-defaults))

(defn -main
  [& [port]]
  (let [port (Integer. (or port
                           (System/getenv "PORT")
                           5001))]
    (jetty/run-jetty #'app {:port  port
                            :join? false})))
