(ns flex.core
  (:require [reagent.core :as r]
            [ajax.core :as ajax]))

(enable-console-print!)

(defn view [params result error]
  [:div
   [:input {:type :text
            :name "str1"
            :placeholder "First String"
            :on-change #(swap! params assoc :str1 (-> % .-target .-value))}]
   [:input {:type :text
            :name "str2"
            :placeholder "Second String"
            :on-change #(swap! params assoc :str2 (-> % .-target .-value))}]
   [:button {:on-click #(do
                          (reset! error nil)
                          (reset! result nil)
                          (ajax/GET
                           "/scramble"
                           {:params @params
                            :handler (fn [response] (reset! result response))
                            :error-handler (fn [response] (reset! error true))}))} "Scramble"]])

(defn main []
  (let [params (r/atom {})
        result (r/atom nil)
        error (r/atom nil)]
    (fn []
      [:div
       (when-not (= nil @error)
         [:div {:style {:color :red}} [:strong "Please enter correct values"]])
       [view params result error]
       (when-not (= nil @result)
         [:div "result is: " @result])])))


(r/render [main]
          (. js/document (getElementById "app")))
