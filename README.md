# flex

Code challenge for flexiana

#### How to run it?
1. clone repo
2. `cd flex`
3. `lein run`
4. `lein figwheel`
5. default browser should open on `http://localhost:5000/index.html`
