(ns flex.main-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [flex.server.main :refer :all]))

(deftest test-app
  (testing "main route"
    (let [response (app (mock/request :get "/scramble?str1=abc&str2=cba"))]
      (is (= (:status response) 200))
      (is (= (:body response) "true"))))

  (testing "not-found route"
    (let [response (app (mock/request :get "/invalid"))]
      (is (= (:status response) 404)))))
