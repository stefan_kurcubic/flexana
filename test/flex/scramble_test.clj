(ns flex.scramble-test
  (:require [clojure.test :refer :all]
            [flex.server.scramble :refer :all]))

(deftest test-scramble
  (testing "scramble?"
    (is (= (scramble? "rekqodlw" "world") true))
    (is (= (scramble? "cedewaraaossoqqyt" "codewars") true))
    (is (= (scramble? "katas" "steak") false))))
